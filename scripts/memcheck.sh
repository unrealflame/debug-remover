docker_image_name="dockerimage"

read -r -d '' COMMANDS << EOM
	cd /test/;
	make clean debug;
	valgrind --leak-check=full --track-origins=yes ./dbrm test.c;
EOM

docker run -ti -v $PWD:/test $docker_image_name bash -c "$COMMANDS"
