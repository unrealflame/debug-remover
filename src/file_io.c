#include <stdio.h>
#include <stdlib.h>

#include "file_io.h"

char* read_file(const char* filepath) {
	// Open the file
	FILE* f = fopen(filepath, "r");

	// Check that the file opened correctly
	if (f == NULL) {
		fprintf(stderr, "The file '%s' failed to open correctly for reading.\n", filepath);
		exit(1);
	}

	// Check that we can seek to the end of the file
	if (fseek(f, 0L, SEEK_END) != 0) {
		// File failed to seek to the end correctly
		fprintf(stderr, "The file '%s' failed to seek to the end correctly.\n", filepath);
		fclose(f);
		exit(1);
	}

	// Get the size of the file
	long buffer_size = ftell(f);
	// Check this was performed correctly
	if (buffer_size == -1) {
		fprintf(stderr, "Failed to get the size of '%s' correctly.\n", filepath);
		fclose(f);
		exit(1);
	}

	// Create a buffer large enough for the whole file
	char* lines = malloc(sizeof(char) * (buffer_size + 1));

	// Return to the beginning of the file
	if (fseek(f, 0L, SEEK_SET) != 0) {
		fprintf(stderr, "Failed to return to the beginning of '%s'.\n", filepath);
		free(lines);
		fclose(f);
		exit(1);
	}

	// Read the whole file into memory
	fread(lines, sizeof(char), buffer_size, f);

	// Check that this was successful
	if (ferror(f)) {
		fprintf(stderr, "Failed to read the whole of '%s'.\n", filepath);
		free(lines);
		fclose(f);
		exit(1);
	}

	// Null terminate the buffer
	lines[buffer_size] = '\0';
	// Close the file pointer
	fclose(f);
	// Return the pointer to lines
	return lines;
}

void write_file(const char* filepath, char** lines) {
	// Open the file
	FILE* f = fopen(filepath, "w");

	// Check it opened correctly
	if (f == NULL) {
		fprintf(stderr, "The file '%s' failed to open correctly for writing.\n", filepath);
		exit(1);
	}

	// Iterate through the lines of the file
	for (int i = 0; lines[i] != NULL; ++i) {
		// Write the line to the file
		fprintf(f, "%s\n", lines[i]);
	}

	// Close the file pointer
	fclose(f);
}
