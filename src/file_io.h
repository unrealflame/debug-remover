// Takes a filepath char* and returns a char* pointing to the 
// entire contents of the file (if it fits into memory)
char* read_file(const char*);

// Takes a filepath and a pointer to an array of strings that 
// should be written to the file, ending in a NULL pointer
void write_file(const char*, char**);
