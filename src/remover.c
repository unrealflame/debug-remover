#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "remover.h"
#include "file_io.h"
#include "string_operations.h"

int main(int argc, const char* argv[]) {
	// Make sure the user has entered a filepath
	if (argc == 1) {
		fprintf(stderr, "Please enter a filepath for the program to use.\n");
		exit(1);
	}

	// Assume the second parameter of argv is the filepath
	const char* filepath = argv[1];

	char* lines = read_file(filepath);
	char** split = split_string(lines, '\n');

	for (int i = 0; split[i] != NULL; ++i) {
		printf("%d %s\n", i, split[i]);
	}

	char** updated = remove_debug_statements(split);

	write_file("updated.c", updated);

	for (int i = 0; split[i] != NULL; ++i) {
		free(split[i]);
	}

	free(lines);
	free(split);
	free(updated);

	return 0;
}

char** remove_debug_statements(char** original) {
	// Find how many lines are in the file and to be removed
	size_t lines = 0, removeable = 0;

	for (size_t i = 0; original[i] != NULL; ++i) {
		if (contains(original[i], "Debug")) {
			// Add 2 as we want to remove the next line as well
			removeable += 2;
		}

		++lines;
	}

	size_t updated_line_count = lines - removeable;
	// Allocate memory for the updated file
	char** updated = malloc(sizeof(char*) * (updated_line_count + 1));

	// Allow us to track which ones to remove
	size_t last_was_debug = 0;
	// Track the number of lines we have added
	size_t line_index = 0;

	// Iterate through the lines and remove the ones we don't want
	for (size_t i = 0; original[i] != NULL; ++i) {
		if (contains(original[i], "Debug")) {
			last_was_debug = 1;
		}
		else if (last_was_debug) {
			last_was_debug = 0;
		} else {
			// If this line was not a debug line and neither was the one
			// before, add this line to the final output
			updated[line_index++] = original[i];
		}
	}

	// Assign the NULL pointer on the end
	updated[updated_line_count] = NULL;

	return updated;
}
