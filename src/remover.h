// Takes a char** that is assumed to be C code and 
// returns a char** which has had the '// Debug' lines 
// and the ones after them removed
char** remove_debug_statements(char**);
