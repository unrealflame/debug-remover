#include <stdio.h>
#include <stdlib.h>

#include "string_operations.h"

size_t length(const char* input) {
	size_t i;
	for (i = 0; input[i] != '\0'; ++i) { }
	return i;
}

char* substring(const char* input, size_t start, size_t end) {
	// Check that end is not out of the bounds of the string
	if (length(input) < end) {
		fprintf(stderr, "The following interval [%zu, %zu) is out of bounds for string '%s'",
				start, end, input);
	}

	// Get the size of the substring
	size_t len = end - start;

	// Allocate memory
	char* sub = malloc(sizeof(char) * (len + 1));

	// Iterate through the string and assign
	for (size_t i = 0, j = start; j < end; ++i, ++j) {
		sub[i] = input[j];
	}

	sub[len] = '\0';

	return sub;
}

char** split_string(const char* input, char delimiter) {
	int last = 0;
	// Count the delimiters
	int delimiter_count = 0;
	for (int i = 0; input[i] != '\0'; ++i) {
		if (input[i] == delimiter) ++delimiter_count;
	}
	
	// Allocate memory for the lines
	char** split = malloc(sizeof(char*) * (delimiter_count + 1));
	// Track the number of split strings we have so far
	int split_index = 0;

	for (int i = 0; input[i] != '\0'; ++i) {
		if (input[i] == delimiter) {
			split[split_index++] = substring(input, last, i);
			last = i + 1;
		}
	}

	split[delimiter_count] = NULL;

	return split;
}

int contains(const char* input, const char* find) {
	// Get the length of the two strings
	size_t input_len = length(input);
	size_t find_len = length(find);

	if (input_len < find_len) return 0;

	// Diff is now guaranteed to be larger or equal 0
	size_t diff = input_len - find_len;
	
	for (size_t i = 0; i < input_len - diff; ++i) {
		// Check each character
		int match = 1;

		// Iterate through the rest of the string and pattern match
		for (size_t j = 0; j < diff; ++j) {
			if (input[i + j] != find[j]) {
				// If a character is not the same, end this call
				match = 0;
				break;
			}
		}

		if (match) return 1;
	}

	return 0;
}
