// Calculates the length of a string (provided it is null terminated)
size_t length(const char*);

// Takes a string, a start index and an end index and 
// returns a pointer to the substring in the range [start, end)
char* substring(const char*, size_t, size_t);

// Takes a string and a delimiter and returns an array of strings
// without the delimiter in them
// Note - 	If two delimiters are next to each other, an empty string 
// 		is returned
char** split_string(const char*, char);

// Takes 2 strings, a main string and a string to find and returns 
// 1 if the main string contains the other, 0 otherwise
int contains(const char*, const char*);
